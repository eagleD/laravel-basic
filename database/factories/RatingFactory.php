<?php

namespace Database\Factories;

use App\Models\Rating;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Product;

class RatingFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Rating::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'product_id' => User::factory()->create()->id,
      'user_id' => Product::factory()->create()->id,
      'rating' => rand(1, 5),
      'comment' => $this->faker->paragraph,
      'status' => 1
    ];
  }
}
