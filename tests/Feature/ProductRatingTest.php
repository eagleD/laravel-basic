<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Rating;
use App\Models\User;
use App\Models\Product;

class ProductRatingTest extends TestCase
{
    public function testsRatingsAreCreatedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $product = Product::factory()->create();
        $comment = "this is a comment";
        $payload = [
            'product_id' => $product->id,
            'rating' => 1,
            'comment' => $comment
        ];

        $this->json('POST', '/api/ratings', $payload, $headers)
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'id' => 1, 
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'rating' => 1,
                    'comment' => $comment, 
                    'status' => 1
                ]
            ]);
    }

    public function testsRatingsAreUpdatedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $product = Product::factory()->create();
        $rating = Rating::factory()->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'rating' => 5
        ]);

        $payload = [
            'product_id' => $product->id,
            'comment' => 'Lorem Ipsum',
            'rating' => 1
        ];

        $response = $this->json('PUT', '/api/ratings/' . $rating->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([ 
                'data' => [
                    'id' => 1, 
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'rating' => 1,
                    'comment' => 'Lorem Ipsum', 
                    'status' => 1
                ]
            ]);
    }

    public function testsRatingsAreDeletedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $rating = Rating::factory()->create();

        $this->json('DELETE', '/api/ratings/' . $rating->id, [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'success', 'message', 'data'
            ]);
    }

    public function testRatingsAreListedCorrectly()
    {
        $product = Product::factory()->create();

        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        Rating::factory()->create([
            'rating' => 1,
            'product_id' => $product->id,
            'user_id' => $user->id,
            'comment' => 'First'
        ]);

        Rating::factory()->create([
            'rating' => 3,
            'product_id' => $product->id,
            'user_id' => $user->id,
            'comment' => 'Second'
        ]);

        $response = $this->json('GET', '/api/ratings', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [ 'product_id' => $product->id, 'rating' => 1, 'comment' => 'First' ],
                    [ 'product_id' => $product->id, 'rating' => 3, 'comment' => 'Second' ],
                ]
            ])
            ->assertJsonStructure([
                'success', 
                'message', 
                'data' => [ 
                    ['id', 'product_id', 'user_id', 'rating', 'comment', 'status']
                 ]
            ]);
    }
}
