<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class LogoutTest extends TestCase
{
    public function testUserIsLoggedOutProperly()
    {
        $user = User::factory()->create(['email' => 'user@test.com']);
        $token = $user->createToken('auth_token')->plainTextToken;
    
        $headers = ['Authorization' => "Bearer $token"];
        
        $this->json('get', '/api/products', [], $headers)->assertStatus(200);
        $this->json('post', '/api/logout', [], $headers)->assertStatus(200);

        $user = User::find($user->id);

        $this->assertEquals(0, count($user->tokens));
    }

    public function testUserWithNullToken()
    {
        // Simulating login
        $user = User::factory()->create(['email' => 'user@test.com']);
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        // Simulating logout
        $user->tokens()->delete();

        $this->json('get', '/api/products', [], $headers)->assertStatus(401);
    }
}
