<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Product;

class ProductTest extends TestCase
{
    public function testsProductsAreCreatedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'Lorem',
            'description' => 'Ipsum',
        ];

        $this->json('POST', '/api/products', $payload, $headers)
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'id' => 1, 
                    'name' => 'Lorem',
                    'description' => 'Ipsum',
                    'active' => 1
                ]
            ]);
    }

    public function testsProductsAreUpdatedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];
        $product = Product::factory()->create([
            'name' => 'First Product',
            'description' => 'First Product Desc',
        ]);

        $payload = [
            'name' => 'Lorem',
            'description' => 'Ipsum',
        ];

        $response = $this->json('PUT', '/api/products/' . $product->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([ 
                'data' => [
                    'id' => 1, 
                    'name' => 'Lorem',
                    'description' => 'Ipsum',
                ]
            ]);
    }

    public function testsProductsAreDeletedCorrectly()
    {
        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];
        $product = Product::factory()->create([
            'name' => 'First Product',
            'description' => 'First Product Desc',
        ]);

        $this->json('DELETE', '/api/products/' . $product->id, [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                'success', 'message', 'data'
            ]);
    }

    public function testProductsAreListedCorrectly()
    {
        Product::factory()->create([
            'name' => 'First Product',
            'description' => 'First Product Desc',
        ]);

        Product::factory()->create([
            'name' => 'Second Product',
            'description' => 'Second Product Desc',
        ]);

        $user = User::factory()->create();
        $token = $user->createToken('auth_token')->plainTextToken;
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/products', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [ 'name' => 'First Product', 'description' => 'First Product Desc' ],
                    [ 'name' => 'Second Product', 'description' => 'Second Product Desc' ]
                ]
            ])
            ->assertJsonStructure([
                'success', 
                'message', 
                'data' => [ 
                    ['id', 'name', 'description', 'active']
                 ]
            ]);
    }
}
